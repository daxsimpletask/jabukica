import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class JabukeService{

    constructor(private http:Http){}

    getJabukeObservable(){
        return this.http.get('http://localhost:3000/jabuke').map((response)=>{
            return response.json();
        });
    }

    getJednaJabukaObservable(id){
        return this.http.get('http://localhost:3000/jabuke/'+id).map((response)=>{
            return response.json();
        });
    }

    addJednaJabukaObservable(jabuka){
        return this.http.post('http://localhost:3000/jabuke',jabuka).map((response)=>{
            return response.json();
        });
    }

    updateJednaJabukaObservable(jabuka){
        return this.http.put('http://localhost:3000/jabuke/'+jabuka.id,jabuka).map((response)=>{
            return response.json();
        });
    }

    deleteJednaJabukaObservable(id){
        return this.http.delete('http://localhost:3000/jabuke/'+id).map((response)=>{
            return response.json();
        });
    }




}