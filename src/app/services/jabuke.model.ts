import { Injectable } from '@angular/core';
import { Jabuka } from '../interfaces/jabuka';
import { JabukeService } from './jabuke.service';

@Injectable()
export class JabukeModel {


    jabuke = [];

    constructor(private service: JabukeService) {
        this.refreshujJabuke();
    }

    refreshujJabuke(){
        this.service.getJabukeObservable().subscribe((jabuke) => {
            this.jabuke = jabuke;
        });
    }

    refreshujJabukeSaClbk(clbk){
        this.service.getJabukeObservable().subscribe((jabuke) => {
            this.jabuke = jabuke;
            clbk();
        });
    }

    obrisiJabuku(id, clbk) {
        this.service.deleteJednaJabukaObservable(id).subscribe(() => {
            this.refreshujJabuke();
            clbk();
        });
    }

    dodajJabuku(jabuka,clbk){
       this.service.addJednaJabukaObservable(jabuka).subscribe((jabuka)=>{
            this.jabuke.push(jabuka);
            clbk();
       })
    }

    dajMiJabukuPoIdu(id,clbk){
        if(this.jabuke && this.jabuke.length > 0){
            for(var i = 0; i < this.jabuke.length;i++){
                if(this.jabuke[i].id == id){
                    clbk(this.jabuke[i])
                }
            }
        }else{
            this.refreshujJabukeSaClbk(()=>{
                for(var i = 0; i < this.jabuke.length;i++){
                    if(this.jabuke[i].id == id){
                        clbk(this.jabuke[i])
                    }
                }
            })
        }
        
    }

    updateJabuka(jabuka, clbk) {
        this.service.updateJednaJabukaObservable(jabuka).subscribe(jabuka => {
          this.refreshujJabuke();
          clbk();
        });
      }

}