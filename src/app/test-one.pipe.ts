import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'testOne'
})
export class TestOnePipe implements PipeTransform {

  transform(value: any, a, b): any {
    value.push(a);
    value.push(b);
    return value;
  }

}
