import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { JabukeService } from './services/jabuke.service';
import { JabukeModel } from './services/jabuke.model';

import { JednaJabukaInfo } from './routes/jednajabuka/info';
import { JednaJabukaEdit } from './routes/jednajabuka/edit';
import { JednaJabukaDelete } from './routes/jednajabuka/delete';

import { Menu } from './components/menu';

import { HomeView } from './routes/HomeView';
import { JabukeView } from './routes/JabukeView';
import { JednaJabuka } from './routes/JednaJabuka';

import { NewJabuka } from './routes/new.jabuka';
import { SliciceComponent } from './slicice/slicice.component';
import { SearchPipe } from './search.pipe';
import { TestOnePipe } from './test-one.pipe';
import { TestTwoPipe } from './test-two.pipe';

const routes: Routes = [
  { path: '', component: HomeView },
  { path: 'slicice', component: SliciceComponent},
  { path: 'jabuke', component: JabukeView },
  { path: 'jabuka/new', component: NewJabuka},
  {
    path: 'jabuke/:id', component: JednaJabuka,
    children: [
      { path: '', redirectTo: 'info', pathMatch: 'full' },
      { path: 'edit', component: JednaJabukaEdit },
      { path: 'info', component: JednaJabukaInfo },
      { path: 'delete', component: JednaJabukaDelete }
    ]
  },
  { path: 'dashboard', redirectTo: '/jabuke' },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  declarations: [
    AppComponent,
    HomeView,
    JabukeView,
    JednaJabuka,
    Menu,
    JednaJabukaInfo,
    JednaJabukaEdit,
    NewJabuka,
    JednaJabukaDelete,
    SliciceComponent,
    SearchPipe,
    TestOnePipe,
    TestTwoPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpModule,
    FormsModule
  ],
  providers: [
    JabukeService,
    JabukeModel
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
