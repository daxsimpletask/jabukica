export interface Jabuka{
    id:number,
    nutritionValue:number,
    weight:string,
    color?:string
}