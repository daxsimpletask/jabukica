import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(niz: any[], searchQuery: any, nova: any): any {

    return searchQuery ? niz.filter(
      (vocka) => {
        return (String(vocka.nutritionValue)
          .toLowerCase()
          .indexOf(searchQuery.toLowerCase()) > -1) ||  (String(vocka.weight)
          .toLowerCase()
          .indexOf(searchQuery.toLowerCase()) > -1) ||  (String(vocka.color)
          .toLowerCase()
          .indexOf(searchQuery.toLowerCase()) > -1);
      }
    ) : niz;
  }

}
