import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    templateUrl:'./jedna.jabuka.html'
})
export class JednaJabuka{

    id;

    constructor(private route:ActivatedRoute){  
        this.route.params.subscribe((paramsObject)=>{
            this.id = paramsObject['id'];
        })
    }


}