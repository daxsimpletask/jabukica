import { Component } from '@angular/core';
import { JabukeModel } from '../services/jabuke.model';
import { Router } from '@angular/router';

import { colors } from '../configs/colors';

@Component({
    templateUrl: './new.jabuka.html'
})
export class NewJabuka {
    boje = colors;
    

    constructor(private model:JabukeModel, private router:Router){}

    jabuka = {};

    dodajJabuku(){
        if(this.jabuka['weight'] && this.jabuka['nutritionValue']){
            this.model.dodajJabuku(this.jabuka, ()=>{
                this.router.navigate(['/jabuke'])
            });
        }else{
            alert('Nije sve definisano')
        }
    }
}