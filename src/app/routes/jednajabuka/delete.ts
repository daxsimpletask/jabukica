import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JabukeModel } from '../../services/jabuke.model';

@Component({
    templateUrl: './delete.html'
})
export class JednaJabukaDelete implements OnInit {

    id;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private jabukeModel: JabukeModel){}

    ngOnInit() {
        this.route.parent.params.subscribe((params) => {
            this.id = params.id
        });

    }

    obrisiJabuku() {
        this.jabukeModel.obrisiJabuku(this.id,() => {
            this.router.navigate(['/jabuke']);
        });
        
    }

}
