

import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { colors } from "../../configs/colors";
import { JabukeModel } from "../../services/jabuke.model";

@Component({
  templateUrl: "./edit.html"
})
export class JednaJabukaEdit implements OnInit {
  id;

  constructor(
    private route: ActivatedRoute,
    private ruter: Router,
    private model: JabukeModel
  ) {}

  jabuka = {};

  colors = colors;

  ngOnInit() {
    this.route.parent.params.subscribe(params => {
      //this.id=params.id
      this.model.dajMiJabukuPoIdu(params.id, a => {
        this.jabuka = a;
      });
    });
  }
  sacuvajJabuku(form) {
      console.log(form);
    this.model.updateJabuka(this.jabuka, () => {
      this.ruter.navigate(["/banane"]);
    });
  }
}