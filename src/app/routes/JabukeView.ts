import { Component } from '@angular/core';
import { JabukeModel } from '../services/jabuke.model';
import { Router } from '@angular/router';

@Component({
    templateUrl:'./jabuke.html'
})
export class JabukeView{

    a = [1,2,3,4,5];

    filtriraneJabuke = [];

    tableConfig = [
        {
            key:'id',
            label:'#',
            type:'text'
        },
        {
            key:'nutritionValue',
            label:'Nutrition Vl.',
            type:'text'
        },
        {
            key:'zrela',
            label:'Je li zrela',
            type:'colorIndicator'
        },
        {
            key:'weight',
            label:'Weight',
            type:'text'
        },
        {
            key:'color',
            label:'Color',
            type:'colorIndicator'
        },
    ]

    filterValues = {

    }

    getAllPossibilitiesForProperty(key){
        var result = [];
        for(var i =0; i< this.filtriraneJabuke.length;i++){
            var color = {
                value:this.filtriraneJabuke[i][key]
            };
            if(!result.includes(color)){
                result.push(color)
            }

        }

        return result;
    }

    constructor(public jabukeModel:JabukeModel, private router:Router){
        this.jabukeModel.refreshujJabukeSaClbk(()=>{
            this.filtriraneJabuke = this.jabukeModel.jabuke;
        });
    }

    filtrirajJabuke(){
        console.log(this.filterValues)
        this.filtriraneJabuke = this.jabukeModel.jabuke;
        this.filtriraneJabuke = this.filtriraneJabuke.filter((jabuka)=>{
            var result = true;
            for(var key in this.filterValues){
                if(String(jabuka[key]).toLowerCase().indexOf(this.filterValues[key].toLowerCase()) > -1){
                    result = result && true;
                }else{
                    result = false;
                }
            }
            return result;
        });       
    }

    otvoriJabuku(id){
        this.router.navigate(['/jabuke',id]);
    }

    editujJabuku(id){
        this.router.navigate(['/jabuke', id , 'edit']);
    }

    obrisiJabuku(id){
        this.router.navigate(['/jabuke', id , 'delete']);
    }



}