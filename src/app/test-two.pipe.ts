import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'testTwo'
})
export class TestTwoPipe implements PipeTransform {

  transform(value: any, a, b): any {
    value.push(a,a,a)
    return value;
  }

}
