import { Component } from '@angular/core';

@Component({
    selector:'menu-list',
    templateUrl:'./menu.html'
})
export class Menu{
    links = [
        {
            naziv:'Home',
            putanja:'/',
            jesamLiAktivan:false
        },
        {
            naziv:'Jabuke',
            putanja:'/jabuke',
            jesamLiAktivan:false
        },
        {
            naziv:'Dodaj Jabuku',
            putanja:'/jabuka/new',
            jesamLiAktivan:false
        }
    ]

}